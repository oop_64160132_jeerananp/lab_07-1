package com.jeeranan132.week8;

public class RectangleShape {
    private String name;
    private double width;
    private double height;

    public RectangleShape(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double Area() {
        double rectA = width * height;
        return rectA;
    }

    public double Grith() {
        double rectG = 2 * (width * height);
        return rectG;
    }

    public void print() {
        System.out.println("Rectangle Area = " + Area());
        System.out.println("Rectangle Girth = " + Grith());
    }
}
