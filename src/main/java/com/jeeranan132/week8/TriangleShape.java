package com.jeeranan132.week8;

public class TriangleShape {
    private String name;
    private double a;
    private double b;
    private double c;

    public TriangleShape(String name, double a, double b, double c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String getName() {
        return name;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public double Area() {
        double s = a + b + c;
        double triA = Math.sqrt((s * (s - a)) * (s - b) * (s - c));
        return triA;
    }

    public double Grith() {
        double triG = a + b + c;
        return triG;
    }

    public void print() {
        System.out.println("Triangle Area = " + Area());
        System.out.println("Triangle Girth = " + Grith());
    }

}
