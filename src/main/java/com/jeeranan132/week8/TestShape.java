package com.jeeranan132.week8;

public class TestShape {
    public static void main(String[] args) {
        RectangleShape rect1 = new RectangleShape("rect1", 10, 5);

        RectangleShape rect2 = new RectangleShape("rect2", 5, 3);

        CircleShape circle1 = new CircleShape("circle1", 1);

        CircleShape circle2 = new CircleShape("circle2", 2);

        TriangleShape tri1 = new TriangleShape("tri2", 5, 5, 6);

        System.out.println("Rectangle Area1 = " + rect1.Area());
        System.out.println("Rectangle Area2 = " + rect2.Area());
        System.out.println("Circle Area1 = " + circle1.Area());
        System.out.println("Circle Area2 = " + circle2.Area());
        System.out.println("Triangle Area1 = " + tri1.Area());

        System.out.println("Rectangle Grith1 = " + rect1.Grith());
        System.out.println("Rectangle Grith2 = " + rect2.Grith());
        System.out.println("Circle Grith1 = " + circle1.Girth());
        System.out.println("Circle Grith2 = " + circle2.Girth());
        System.out.println("Triangle Grith1 = " + tri1.Grith());
    }

}
