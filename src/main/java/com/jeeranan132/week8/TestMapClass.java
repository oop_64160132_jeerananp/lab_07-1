package com.jeeranan132.week8;

public class TestMapClass {
    private String name;
    private double width;
    private double height;

    public TestMapClass(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void print() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (j <= width) {
                    System.out.print("-");
                }
            }
            System.out.println();
        }

    }
}
