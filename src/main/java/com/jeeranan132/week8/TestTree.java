package com.jeeranan132.week8;

public class TestTree {
    public static void main(String[] args) {
        TestTreeClass Tree1 = new TestTreeClass("Tree1", 5, 10);
        TestTreeClass Tree2 = new TestTreeClass("Tree2", 5, 11);

        Tree1.print1();
        Tree2.print1();
        for (int y = 1; y < 15; y++) {
            for (int x = 1; x < 15; x++) {
                if (Tree1.getX() == x & Tree1.getY() == y) {
                    System.out.print(Tree1.getSymbol());
                    continue;
                }
                if (Tree2.getX() == x & Tree2.getY() == y) {
                    System.out.print(Tree2.getSymbol());
                    continue;
                }
                System.out.print('-');
            }
            System.out.println();
        }
    }
}
