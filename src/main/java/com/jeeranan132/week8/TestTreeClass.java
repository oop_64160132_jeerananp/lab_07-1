package com.jeeranan132.week8;

public class TestTreeClass {
    private String name;
    private int x;
    private int y;
    private char symbol;

    public TestTreeClass(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.symbol = 'T';

    }

    public String getName() {
        return name;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public void print1() {
        System.out.println("Tree " + symbol + " (" + x + "," + y + ")");
    }
}
